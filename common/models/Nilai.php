<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "nilai".
 *
 * @property int $id
 * @property string $nim
 * @property string $nidn
 * @property string $kd_makul
 * @property int $nped
 * @property int $nprof
 * @property int $nkep
 * @property int $nsos
 */
class Nilai extends \yii\db\ActiveRecord
{
    public $ped1, $ped2, $ped3, $ped4, $ped5, $ped6, $ped7, $ped8, $ped9;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'nilai';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nim', 'nidn', 'kd_makul', 'nped', 'nprof', 'nkep', 'nsos'], 'required'],
            [['nped', 'nprof', 'nkep', 'nsos'], 'integer'],
            [['nim', 'nidn', 'kd_makul'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'nim' => 'NIM',
            'nidn' => 'NIDN',
            'kd_makul' => 'Matakuliah',
            'nped' => 'Aspek Pedagogik',
            'nprof' => 'Aspek Profesionalitas',
            'nkep' => 'Aspek Kepribadian',
            'nsos' => 'Aspek Sosial',
        ];
    }
}

<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "soal".
 *
 * @property int $id
 * @property string $kode_soal
 * @property string $jenis_soal
 * @property string $soal
 */
class Soal extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'soal';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['kode_soal', 'jenis_soal', 'soal'], 'required'],
            [['soal'], 'string'],
            [['kode_soal', 'jenis_soal'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'kode_soal' => 'Kode Soal',
            'jenis_soal' => 'Jenis Soal',
            'soal' => 'Soal',
        ];
    }
}

<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Nilai */
/* @var $form yii\widgets\ActiveForm */
?>

<style>

td, th{
    border: 1px solid #999;
    padding: 0.5rem;
}
.css-serial{
    counter-reset: serial-number;
}

.css-serial td:first-child:before{
    counter-increment: serial-number;
    content: counter(serial-number);
}

</style>

<div class="nilai-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'nim')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'nidn')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'kd_makul')->textInput(['maxlength' => true]) ?>


    <form>
    <table class="css-serial">
    <thead>
        <tr>
            <th>No</th>
            <th>Soal</th>
            <th>Jawaban</th>
        </tr>
    </thead>   

    <?php
    foreach($soal as $v)
    {
    ?>        

    <tr>
        <td></td>
        <td><?= $v->soal ?></td>

    
        <td>
            <input type="radio" name="jwb<?= $v->id;?>" value="1">1
            <input type="radio" name="jwb<?= $v->id;?>" value="2">2
            <input type="radio" name="jwb<?= $v->id;?>" value="3">3
            <input type="radio" name="jwb<?= $v->id;?>" value="4">4
            <input type="radio" name="jwb<?= $v->id;?>" value="5">5
        </td>
       
    </tr>

    <?php
    }
    ?>

    </table>
    </form>

    

    <p></p>
    <p></p>


    <!-- <?= $form->field($model, 'nped')->textInput() ?>

    <?= $form->field($model, 'nprof')->textInput() ?>

    <?= $form->field($model, 'nkep')->textInput() ?>

    <?= $form->field($model, 'nsos')->textInput() ?> -->

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

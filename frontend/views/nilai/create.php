<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\grid\Column;


/* @var $this yii\web\View */
/* @var $model common\models\Nilai */

$this->title = 'Kuesioner Evaluasi Kinerja Dosen';
$this->params['breadcrumbs'][] = ['label' => 'Nilais', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="nilai-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        
        'model' => $model,
        'soal' => $soal,
    ]) ?>

    <!-- <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns'=>[
            ['class' => 'yii\grid\SerialColumn'],
            'soal',

            [
                'class' => 'yii\grid\RadioButtonColumn',
                // 'radioOptions' => function ($model) {
                //     return[
                //         'value' => $model['value']
                //     ];
                // }
            ],
        ]
    ]);
    ?> -->
</div>

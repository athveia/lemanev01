<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "admin_status".
 *
 * @property int $level
 * @property string $admin_status
 *
 * @property User[] $users
 */
class AdminStatus extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'admin_status';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['level', 'admin_status'], 'required'],
            [['level'], 'integer'],
            [['admin_status'], 'string', 'max' => 255],
            [['level', 'admin_status'], 'unique', 'targetAttribute' => ['level', 'admin_status']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'level' => 'Level',
            'admin_status' => 'Admin Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUsers()
    {
        return $this->hasMany(User::className(), ['level' => 'level']);
    }
}

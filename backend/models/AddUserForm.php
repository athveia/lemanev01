<?php
namespace backend\models;

use yii\base\Model;
use common\models\User;
use backend\models\AdminStatus;

/**
 * Signup form
 */
class AddUserForm extends Model
{
    public $username;
    public $email;
    public $password;
    public $name;
    public $admin_status;
    public $level;

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            ['username', 'trim'],
            ['username', 'required'],
            ['username', 'unique', 'targetClass' => '\common\models\User', 'message' => 'This username has already been taken.'],
            ['username', 'string', 'min' => 2, 'max' => 255],
            
            ['name', 'string'],
            ['level', 'integer'],

            ['email', 'trim'],
            ['email', 'required'],
            ['email', 'email'],
            ['email', 'string', 'max' => 255],
            ['email', 'unique', 'targetClass' => '\common\models\User', 'message' => 'This email address has already been taken.'],

            ['password', 'required'],
            ['password', 'string', 'min' => 6],
        ];
    }

    /**
     * Signs user up.
     *
     * @return User|null the saved model or null if saving fails
     */
    public function adduser()
    {

        
        $user = new User();
        $user->username = $this->username;
        $user->name = $this->name;
        $user->email = $this->email;
        $user->level = $this->level;
        $user->setPassword($this->password);
        $user->generateAuthKey();
        
        if (!$this->validate()) {
            return null;
        }
        
        
        return $user->save() ? $user : null;
        
        
    }
    
    public function getLevel0()
    {
        return $this -> hasOne(AdminStatus::className(), ['level' => 'level']);
    }
}
?>

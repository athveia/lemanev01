<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "jadwal".
 *
 * @property string $kd_makul
 * @property string $nama_makul
 * @property string $smt
 * @property string $pengajar
 *
 * @property Dosen $pengajar0
 */
class Jadwal extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'jadwal';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['kd_makul', 'nama_makul', 'smt', 'pengajar'], 'required'],
            [['kd_makul', 'nama_makul', 'smt', 'pengajar'], 'string', 'max' => 255],
            [['kd_makul'], 'unique'],
            [['pengajar'], 'exist', 'skipOnError' => true, 'targetClass' => Dosen::className(), 'targetAttribute' => ['pengajar' => 'nidn']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'kd_makul' => 'Kode Matakuliah',
            'nama_makul' => 'Nama Matakuliah',
            'smt' => 'Semester',
            'pengajar' => 'Pengajar',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPengajar0()
    {
        return $this->hasOne(Dosen::className(), ['nidn' => 'pengajar']);
    }
}

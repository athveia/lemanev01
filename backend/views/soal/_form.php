<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
//use yii\jui\AutoComplete;
//use yii\web\JsExpression;

/* @var $this yii\web\View */
/* @var $model common\models\Soal */
/* @var $form yii\widgets\ActiveForm */
?>


<div class="soal-form">

        <?php $form = ActiveForm::begin(); ?>

        <!-- THIS IS THE DROP DOWN LIST -->
        <?= $form->field($model, 'jenis_soal')->dropDownList([
            'pedagogik' => 'Kompetensi Pedagogik', 
            'profesional' => 'Kompetensi Profesional', 
            'kepribadian' => 'Aspek Kepribadian',
            'sosial' => 'Aspek Sosial'
        ], ['prompt' => '-Pilih salah satu-'])
        ?>

        
        <!-- WHEN I SELECT E.G. Kompetensi Pedagogik, it should automatically filled "PED" -->

        <!-- <?= $form->field($model, 'kode_soal')->textInput(['readonly'])?> -->

        <?= $form->field($model, 'soal')->textarea(['rows' => 6]) ?>
        
        

        <div class="form-group">
            <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
        </div>

        <?php ActiveForm::end(); ?>
        
</div>
<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use backend\models\AdminStatus;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model backend\models\UserTable */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-table-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'username')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'password')->passwordInput() ?>

    <?= $form->field($model, 'level')->DropDownList(
                    
    ArrayHelper::map(AdminStatus::find() -> all() , 'level', 'admin_status'), ['prompt'=>'Pilih Jenis User']                
    ) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

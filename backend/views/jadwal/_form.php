<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use backend\models\Dosen;
use yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $model backend\models\Jadwal */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="jadwal-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'kd_makul')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'nama_makul')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'smt')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'pengajar')->DropDownList(
                    
    ArrayHelper::map(Dosen::find() ->asArray() -> all() , 'nidn', 
                     function($model){
                         return $model['nidn'].' - '.$model['nama'];
                     }
                    ), ['prompt'=>'Pilih Dosen'])

    ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
